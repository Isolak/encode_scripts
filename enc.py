#!/usr/bin/env python3

import os
import argparse
import subprocess
import sys
import logging
import datetime
import json
import tqdm
import requests
import yaml


parser = argparse.ArgumentParser(description="Script to encode videos")
parser.add_argument("-p","--path", help="Path to search", type=str, required=True)
parser.add_argument("-ext", "--extension", help="File extension to look for", type=str, required=True)
parser.add_argument("-r", "--remove", help="Remove found files after processing", action="store_true")
parser.add_argument("-s", "--size", help="Files smaller than will be skipped (in MiB) Default is 300MiB", type=int, default=300)
parser.add_argument("-crf", "--crf", help="Set the CRF value for ffmpeg (Default is 24)", type=int, default=24)
parser.add_argument("-hw", "--nvenc", help="Uses nvenc for encoding instead of cpu x265", action="store_true")
args = parser.parse_args()

location = args.path
extension = args.extension
size = args.size*1024*1024
size_mib = args.size
crf = args.crf
ffmpeg_setup = ["ffmpeg", "-hide_banner", "-loglevel", "warning", "-stats"]
ffmpeg_audio = ["-c:a", "libopus", "-b:a", "128k"]
nvenc_setup = ["NVEncC64.exe", "--avhw", "--metadata", "clear", "--avsync", "vfr"]
nvenc_audio = ["--audio-codec", "libopus", "--audio-bitrate", "128"]

log_file = str(datetime.datetime.now().strftime("%Y-%m-%d")) + "_enc.log"

log_path = os.path.join(location, log_file)
logging.basicConfig(encoding="utf-8", level=logging.INFO, format="%(asctime)s: %(message)s", datefmt="%m/%d/%Y %H:%M:%S", handlers=[logging.FileHandler(log_path), logging.StreamHandler(sys.stdout)])

with open("data.yml", 'r') as stream:
    data_loaded = yaml.safe_load(stream)

url = data_loaded["url"]
headers = data_loaded["headers"]
tts_data = {"message": "TTS", "data": {"tts_text": "Encode has finished"}}
data = {"title": "Encode", "message": "Encode has finished", "data": {"notification_icon": "mdi:expansion-card", "image": "/media/local/graphics-card-svgrepo-com.jpg"}}

files_to_process = []
for root, dirs, files in os.walk(location):
    for file in files:
        if file.endswith("." + extension):
            if os.stat(os.path.join(root, file)).st_size >= size:
                files_to_process.append(os.path.join(root, file))
            else:
                logging.info("Skipping " + file + " since it is smaller than " + str(size_mib) + "MiB")

length = len(files_to_process)
progress_bar = tqdm.tqdm(total=length, bar_format="{l_bar}{bar}| {n_fmt}/{total_fmt} [{elapsed},{rate_fmt}{postfix}]")

for item in files_to_process:
    command = []
    file_in = item
    logging.info("Started processing " + os.path.basename(file_in))
    logging.info("File (" + str(int(files_to_process.index(item))+1) + "/" + str(len(files_to_process)) + ")")
    if args.nvenc is True:
        json_command = ["ffprobe", "-v", "quiet", "-print_format", "json", "-select_streams", "v:0", "-show_streams", "-i", file_in]
        file_out = os.path.splitext(file_in)[0] + ".nvenc.mkv"
        bitrate = int(json.loads(subprocess.run(json_command, capture_output=True).stdout)['streams'][0]['bit_rate'])
        cqp = f"{crf}:{crf+2}:{crf+4}"
        #qp = crf
        #qmin = qp + 2
        #qmax = qp - 2
        #ffmpeg_video = ["-c:v", "hevc_nvenc", "-rc", "constqp", "-qp", str(qp), "-qmin", str(qmin), "-qmax", str(qmax), "-rc-lookahead", "250", "-temporal_aq", "1", "-b_ref_mode", "2"]
        #command.extend(ffmpeg_setup)
        #command.extend(["-i", file_in])
        #command.extend(ffmpeg_video)
        #command.extend(ffmpeg_audio)
        #command.append(file_out)
        #nvenc_video = ["-c", "hevc", "--profile", "main10", "--vbr", str(int(bitrate/2/1024)), "--vbr-quality", str(crf), "--multipass", "2pass-full", "--preset", "P7"]
        nvenc_video = ["-c", "hevc", "--profile", "main10", "--cqp", str(cqp), "--lookahead", "32", "--aq-temporal", "--preset", "P7"]
        command.extend(nvenc_setup)
        command.extend(["-i", file_in])
        command.extend(nvenc_video)
        command.extend(nvenc_audio)
        command.extend(["-o"])
        command.append(file_out)
        #print(command)
    else:
        file_out = os.path.splitext(file_in)[0] + ".mkv"
        ffmpeg_video = ["-map_metadata", "-1", "-c:v", "libx265", "-x265-params", "log-level=error", "-preset:v", "medium", "-profile:v", "main10", "-crf", str(crf), "-pix_fmt", "yuv420p10le"]
        command.extend(ffmpeg_setup)
        command.extend(["-i", file_in])
        command.extend(ffmpeg_video)
        command.extend(ffmpeg_audio)
        command.append(file_out)
    subprocess.run(command)#, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    logging.info("Finished processing " + file_in)
    progress_bar.update(1)
    if args.remove is True:
        logging.info("Removing " + file_in)
        os.remove(file_in)

time_elapsed = progress_bar.format_dict['elapsed']    
progress_bar.close()
print("Took " + f"{time_elapsed:.2f}" + " seconds")

response = requests.post(url, headers=headers, json=data)
#response_tts = requests.post(url, headers=headers, json=tts_data)

#ffmpeg -y -hide_banner -vsync 0 -hwaccel cuda -hwaccel_output_format cuda  -hwaccel_device 0 -c:v:0 h264_cuvid -i "input.mp4" -vf "hwdownload,format=nv12" -c copy -c:v:0 hevc_nvenc -profile:v main10 -pix_fmt p010le -rc:v:0 vbr -tune hq -preset p5 -multipass 1 -bf 4 -b_ref_mode 1 -nonref_p 1 -rc-lookahead 75 -spatial-aq 1 -aq-strength 8 -temporal-aq 1 -cq 21 -qmin 1 -qmax 99 -b:v:0 10M -maxrate:v:0 20M -gpu 0 "output.mkv"
