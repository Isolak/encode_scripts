import json
import subprocess
import sys
import os
import pprint

source = "H:\DVD"
target = "G:\enc\DVD"
files_to_process = []
ffmpeg_setup = ["ffmpeg", "-hide_banner"]
ffmpeg_video = ["-c:v", "libx265", "-preset:v", "medium", "-crf", "20", "-profile:v", "main10", "-pix_fmt", "yuv420p10le"]
location = source

for root, dirs, files in os.walk(location):
    for file in files:
        if file.endswith(".mkv"):
            command = []
            dir = os.path.split(root)[-1]
            #print(os.path.join(target, dir, file))
            file_in = os.path.join(root, file)
            ffmpeg_in = ["-i", file_in]
            file_out = os.path.join(target, dir, file)
            ffmpeg_out = ["-c:s", "copy", "-f", "matroska", file_out]
            probe_cmnd = ["ffprobe", "-v", "quiet", "-print_format", "json", "-show_streams", "-i", file_in]
            json_stuff = json.loads(subprocess.run(probe_cmnd, capture_output=True).stdout)
            file_channels = json_stuff['streams'][1]['channels']
            file_channel_layout = json_stuff['streams'][1]['channel_layout']
            ffmpeg_audio = ["-c:a", "libopus", "-b:a", str(int(int(file_channels)*64)) + "k"]
            if file_channel_layout == "5.1(side)":
                ffmpeg_filter = ["-af", "channelmap=channel_layout=5.1"]
            elif file_channel_layout == "stereo":
                ffmpeg_filter = ["-af", "channelmap=channel_layout=stereo"]
            command.extend(ffmpeg_setup)
            command.extend(ffmpeg_in)
            command.extend(ffmpeg_video)
            command.extend(ffmpeg_audio)
            command.extend(ffmpeg_filter)
            command.extend(ffmpeg_out)
            subprocess.run(command)

#probe_cmnd = ["ffprobe", "-v", "quiet", "-print_format", "json", "-show_streams", "-i", sys.argv[1]]
#print(str(subprocess.run(cmnd, capture_output=True).stdout))
#json = json.loads(subprocess.run(probe_cmnd, capture_output=True).stdout)

#pprint.pprint(json['streams'][1]['channels'])
#pprint.pprint(json['streams'][1]['channel_layout'])
#bitrate = json['streams'][1]
#pprint(bitrate)