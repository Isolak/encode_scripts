import requests
import yaml


with open("data.yml", 'r') as stream:
    data_loaded = yaml.safe_load(stream)


url = data_loaded["url"]
headers = data_loaded["headers"]
tts_data = {"message": "TTS", "data": {"tts_text": "Someone is at the door!"}}
data = {"title": "Encode", "message": "Encode has finished", "data": {"notification_icon": "mdi:expansion-card", "image": "/media/local/graphics-card-svgrepo-com.jpg"}}


response = requests.post(url, headers=headers, json=data)
#response_tts = requests.post(url, headers=headers, json=tts_data)
print(response.text)
#print(response_tts.text)