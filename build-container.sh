#! /usr/bin/env sh

if command -v docker &> /dev/null
then
    echo "Using docker build"
    docker buildx build -t max:test -t max:$(date +'%Y%m%d') -f Dockerfile.ffmpeg .
fi

if command -v podman &> /dev/null
then
    echo "Using podman build"
    podman build -t max:test -t max:$(date +'%Y%m%d') -f Dockerfile.ffmpeg .
fi
